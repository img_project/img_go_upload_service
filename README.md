# Image resizer project upload service

Initial structuring

1. `logger` package
   log levels:
        a. debug - debug level, shows all log messages
        b. info - info level, shows all log info messages
        c. warn - warn level, shows all log warn messages
        d. error - error level, shows all log error messages
        e. panic - panic level, shows all log panic messages
        f. fatal - fatal level, shows all log fatal messages

2. `config` package
   all configurations for running application
   this configurations will be loaded from `.env` file and will convert into golang structure type

3. For running service create `.env` file from `.env.example` and type this command:
   ```bash
   make run
   ```
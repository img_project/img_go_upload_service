package postgres

import (
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/img_project/img_go_upload_service/storage/repository"
)

// create mock upload
func createUpload(t *testing.T, u *repository.Upload) {
	id, err := store.UploadRepo().Create(u)

	assert.NoError(t, err)
	assert.NotEmpty(t, id)

	u.ID = id
}

func TestCreateUpload(t *testing.T) {
	userID, err := uuid.NewRandom()

	assert.NoError(t, err)

	u := &repository.Upload{
		UserID:   userID.String(),
		Name:     "dummy file",
		FileName: "dummy.jpg",
		FilePath: "/tmp/dummy.jpg",
	}

	createUpload(t, u)
}

func TestFindByIDAndUserID(t *testing.T) {
	userID, err := uuid.NewRandom()

	assert.NoError(t, err)

	u := &repository.Upload{
		UserID:   userID.String(),
		Name:     "dummy file",
		FileName: "dummy.jpg",
		FilePath: "/tmp/dummy.jpg",
	}

	createUpload(t, u)

	testCases := []struct {
		name   string
		id     string
		userID string
		valid  bool
	}{
		{
			name:   "valid test",
			id:     u.ID,
			userID: u.UserID,
			valid:  true,
		},
		{
			name:   "invalid test",
			id:     "12323-39203930-32390302",
			userID: "3920-332903920",
			valid:  false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			foundUpload, err := store.UploadRepo().FindByIDAndUserID(tc.id, tc.userID)
			if tc.valid {
				assert.NoError(t, err)
				assert.Equal(t, u.ID, foundUpload.ID)
				assert.Equal(t, u.UserID, foundUpload.UserID)
			} else {
				assert.Equal(t, (*repository.Upload)(nil), foundUpload)
				assert.Error(t, err)
			}
		})
	}
}

func TestGetAll(t *testing.T) {
	userID, err := uuid.NewRandom()
	assert.NoError(t, err)

	for i := 0; i < 10; i++ {
		u := &repository.Upload{
			UserID:   userID.String(),
			Name:     faker.Sentence(),
			FileName: faker.YearString() + ".jpg",
			FilePath: faker.URL(),
		}

		createUpload(t, u)
	}

	count, uploads, err := store.UploadRepo().GetAll(userID.String(), 3, 0)

	assert.NoError(t, err)
	assert.NotEqual(t, ([]*repository.Upload)(nil), uploads)

	assert.Equal(t, len(uploads), 3)
	assert.Equal(t, int64(10), count)

	oldID := uploads[0].ID

	count, uploads, err = store.UploadRepo().GetAll(userID.String(), 3, 3)

	assert.NoError(t, err)
	assert.NotEqual(t, ([]*repository.Upload)(nil), uploads)
	assert.Equal(t, int64(10), count)

	assert.Equal(t, len(uploads), 3)

	assert.NotEqual(t, oldID, uploads[0].ID)
}

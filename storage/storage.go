package storage

import (
	"github.com/jmoiron/sqlx"
	pgRepo "gitlab.com/img_project/img_go_upload_service/storage/postgres"
	"gitlab.com/img_project/img_go_upload_service/storage/repository"
)

// Storage interface for working with models
type Storage interface {
	UploadRepo() repository.UploadRepo
}

// storage implements Storage interface methods
type storage struct {
	db *sqlx.DB
}

// NewPostgresStorage creates new storage instance
func NewPostgresStorage(db *sqlx.DB) Storage {
	return &storage{db}
}

// UploadRepo ...
func (s *storage) UploadRepo() repository.UploadRepo {
	return pgRepo.NewUploadRepo(s.db)
}

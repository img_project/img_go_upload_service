package repository

import (
	"database/sql"
	"errors"
)

var (
	ErrNilValue = errors.New("nil value given")
)

// BaseSchema ...
type BaseSchema struct {
	ID        string       `db:"id"`
	CreatedAt sql.NullTime `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
}

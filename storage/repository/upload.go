package repository

// Upload ...
type Upload struct {
	BaseSchema
	UserID   string `db:"user_id"`
	Name     string `db:"name"`
	FileName string `db:"file_name"`
	FilePath string `db:"file_path"`
}

// UploadRepo user repository interface
type UploadRepo interface {
	Create(*Upload) (string, error)
	GetAll(userID string, limit, offset int64) (int64, []*Upload, error)
	FindByIDAndUserID(id, userID string) (*Upload, error)
}

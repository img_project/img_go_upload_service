package postgres

import (
	"context"
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/img_project/img_go_upload_service/storage/repository"

	imgVar "gitlab.com/img_project/img_go_upload_service/img_variables"
)

type uploadRepo struct {
	db *sqlx.DB
}

func NewUploadRepo(db *sqlx.DB) repository.UploadRepo {
	return &uploadRepo{db: db}
}

func (repo *uploadRepo) GetAll(userID string, limit, offset int64) (int64, []*repository.Upload, error) {
	query := `SELECT COUNT(1) FROM uploads WHERE user_id=$1`

	var count int64

	if err := repo.db.GetContext(context.Background(), &count, query, userID); err != nil {
		return 0, nil, err
	}

	query = `SELECT * FROM uploads WHERE user_id=:user_id LIMIT :limit OFFSET :offset`
	args := map[string]interface{}{
		"user_id": userID,
		"limit":   limit,
		"offset":  offset,
	}

	var uploads []*repository.Upload

	rows, err := repo.db.NamedQueryContext(context.Background(), query, args)

	if err != nil {
		return 0, nil, err
	}

	for rows.Next() {
		u := &repository.Upload{}
		if err := rows.StructScan(u); err != nil {
			return 0, nil, err
		}

		uploads = append(uploads, u)
	}

	return count, uploads, nil
}

func (repo *uploadRepo) FindByIDAndUserID(id, userID string) (*repository.Upload, error) {
	query := `SELECT * FROM uploads WHERE id=:id and user_id=:user_id LIMIT 1`

	args := map[string]interface{}{
		"id":      id,
		"user_id": userID,
	}

	var uploads []*repository.Upload

	rows, err := repo.db.NamedQueryContext(context.Background(), query, args)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		u := &repository.Upload{}
		if err := rows.StructScan(u); err != nil {
			return nil, err
		}

		uploads = append(uploads, u)
	}

	if len(uploads) > 0 {
		return uploads[0], nil
	}

	return nil, imgVar.ErrFileNotFound
}

func (repo *uploadRepo) Create(upload *repository.Upload) (string, error) {
	if upload.UserID == "" {
		return "", errors.New("invalid user id")
	}

	if upload.Name == "" {
		return "", errors.New("invalid name")
	}

	if upload.FileName == "" {
		return "", errors.New("invalid file name")
	}

	if upload.FilePath == "" {
		return "", errors.New("invalid file path")
	}

	query := `
		INSERT INTO uploads (
			id,
			user_id,
			name,
			file_name,
			file_path
		) VALUES (
			:id,
			:user_id,
			:name,
			:file_name,
			:file_path
		)`

	id, err := uuid.NewRandom()

	if err != nil {
		return "", err
	}

	args := map[string]interface{}{
		"id":        id.String(),
		"user_id":   upload.UserID,
		"name":      upload.Name,
		"file_name": upload.FileName,
		"file_path": upload.FilePath,
	}

	tx := repo.db.MustBeginTx(context.Background(), &sql.TxOptions{})

	_, err = tx.NamedExecContext(context.Background(), query, args)

	if err != nil {
		if err := tx.Rollback(); err != nil {
			return "", err
		}

		return "", err
	}

	return id.String(), tx.Commit()
}

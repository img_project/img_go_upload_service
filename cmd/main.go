package main

import (
	"os"

	"gitlab.com/img_project/img_go_upload_service/pkg/config"
	"gitlab.com/img_project/img_go_upload_service/pkg/logger"
	grpcService "gitlab.com/img_project/img_go_upload_service/service/grpc"
)

func main() {
	// Load config from .env
	if err := config.New(os.Getenv("ENVIRONMENT")); err != nil {
		panic(err)
	}

	// Initialize customized zap logger
	logger.New(config.Get().App.LogLevel, config.Get().App.Name)

	// Initialize service
	service, err := grpcService.NewService()

	if err != nil {
		panic(err)
	}

	if err := service.Run(); err != nil {
		panic(err)
	}
}

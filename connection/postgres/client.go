package postgres

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/img_project/img_go_upload_service/pkg/config"
)

func NewPostgres(cfg config.PostgreSQL) (*sqlx.DB, error) {
	var _instance *sqlx.DB

	ctx, cancelFunc := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancelFunc()

	postgresURL := fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.Username,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.DatabaseName,
	)

	_instance, err := sqlx.ConnectContext(ctx, "postgres", postgresURL)

	return _instance, err
}

func ClosePostgres(db *sqlx.DB) error {
	return db.Close()
}

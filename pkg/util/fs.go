package util

import (
	"io/ioutil"
	"os"
	"path"

	"github.com/google/uuid"
)

func SaveStreamedDataToFile(uploadFolder, fileType string, data []byte) (string, string, error) {
	fileID, err := uuid.NewRandom()

	if err != nil {
		return "", "", err
	}

	fileName := fileID.String() + fileType

	filePath := path.Join(uploadFolder, fileName)

	if err := ioutil.WriteFile(filePath, data, 0666); err != nil {
		if os.IsNotExist(err) {
			os.Mkdir(uploadFolder, 0777)

			err = ioutil.WriteFile(filePath, data, 0666)

			if err != nil {
				return "", "", err
			}
		}

		return "", "", err
	}

	return fileName, filePath, nil
}

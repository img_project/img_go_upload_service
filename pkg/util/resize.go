package util

import (
	"bytes"

	"github.com/disintegration/imaging"
)

func Resize(filePath string, width *int64) (*bytes.Reader, error) {
	src, err := imaging.Open(filePath)
	if err != nil {
		return &bytes.Reader{}, err
	}

	if width != nil {
		src = imaging.Resize(src, int(*width), 0, imaging.Lanczos)
	}

	imageBuffer := bytes.Buffer{}
	err = imaging.Encode(&imageBuffer, src, imaging.JPEG)
	if err != nil {
		return &bytes.Reader{}, err
	}

	return bytes.NewReader(imageBuffer.Bytes()), nil
}

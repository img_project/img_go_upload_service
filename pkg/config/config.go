package config

import "github.com/MrWebUzb/goenv"

// Application configurations of application
type Application struct {
	Name          string `env:"APPLICATION" default:"example_service" required:"true"` // application name
	Port          string `env:"SERVICE_PORT" default:":9999"`                          // application running port
	UploadFolder  string `env:"UPLOAD_FOLDER" default:"/tmp/uploads"`                  // application upload path
	MaxUploadSize int64  `env:"MAX_UPLOAD_SIZE" default:"3145728"`                     // application max upload size, default 3 Mb
	LogLevel      string `env:"LOG_LEVEL" default:"debug"`                             // application log level
}

// PostgreSQL connection environment variables
type PostgreSQL struct {
	Host         string `env:"POSTGRES_HOST" default:"localhost"`
	Port         string `env:"POSTGRES_PORT" default:"5432"`
	Username     string `env:"POSTGRES_USER" default:"postgres"`
	Password     string `env:"POSTGRES_PASS" default:""`
	DatabaseName string `env:"POSTGRES_DB" default:"example"`
}

// Database database configurations
type Database struct {
	Postgres PostgreSQL
}

// Config structure of application config parameters
// Application name of the service
// LogLevel level of the logging messages
type Config struct {
	Environment string      // service environment
	App         Application // application configs
	Database    Database    // database configs
}

// Instance of the configurations
var _config *Config = nil

func New(environment string, fileNames ...string) error {
	env, err := goenv.New(fileNames...)

	if err != nil {
		return err
	}

	if _config == nil {
		_config = &Config{
			Environment: environment,
		}

		if err := env.Parse(_config); err != nil {
			return err
		}
	}

	return nil
}

func Get() *Config {
	return _config
}

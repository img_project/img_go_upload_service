package grpc

import (
	"image"
	"io"

	imgVars "gitlab.com/img_project/img_go_upload_service/img_variables"
	"gitlab.com/img_project/img_go_upload_service/pkg/util"
	pbV1 "gitlab.com/img_project/img_go_upload_service/protobuf/upload_service/v1"
	"google.golang.org/grpc/codes"
)

func (s *service) Get(getImageRequest *pbV1.GetImageRequest, stream pbV1.UploadService_GetServer) error {
	upload, err := s.store.UploadRepo().FindByIDAndUserID(getImageRequest.GetId(), getImageRequest.GetUserId())

	if err == imgVars.ErrFileNotFound {
		return logAndReturnError(codes.NotFound, "file not found")
	}
	if err != nil {
		return logAndReturnError(codes.Internal, "database error: %v", err)
	}

	reader, err := util.Resize(upload.FilePath, getImageRequest.Width)

	if err != nil {
		if err == image.ErrFormat {
			return logAndReturnError(codes.NotFound, "file not found")
		}
		return logAndReturnError(codes.Internal, "could not read file: %v", err)
	}

	buffer := make([]byte, imgVars.DefaultChunkSize)

	for {
		if err := contextError(stream.Context()); err != nil {
			return err
		}

		n, err := reader.Read(buffer)
		if err == io.EOF {
			break
		}

		if err != nil {
			return logAndReturnError(codes.Internal, "could not read data from buffer: %v", err)
		}

		request := &pbV1.GetImageResponse{
			Content: buffer[:n],
		}

		err = stream.Send(request)

		if err != nil {
			return logAndReturnError(codes.Internal, "could not send data: %v", err)
		}
	}

	return nil
}

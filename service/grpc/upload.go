package grpc

import (
	"bytes"
	"io"

	"gitlab.com/img_project/img_go_upload_service/pkg/config"
	"gitlab.com/img_project/img_go_upload_service/pkg/logger"
	"gitlab.com/img_project/img_go_upload_service/pkg/util"
	pbV1 "gitlab.com/img_project/img_go_upload_service/protobuf/upload_service/v1"
	"gitlab.com/img_project/img_go_upload_service/storage/repository"
	"google.golang.org/grpc/codes"
)

func (s *service) Upload(stream pbV1.UploadService_UploadServer) error {
	request, err := stream.Recv()

	if err != nil {
		return logAndReturnError(codes.Internal, "could not load info: %v", err)
	}

	uploadInfo := request.GetInfo()

	maxFileSize, fileData, fileSize := int(config.Get().App.MaxUploadSize), bytes.Buffer{}, 0

	for {
		err := contextError(stream.Context())

		if err != nil {
			return err
		}

		logger.Get().Info("receiving data")

		request, err = stream.Recv()
		if err == io.EOF {
			logger.Get().Info("finished reading data")
			break
		}
		if err != nil {
			return logAndReturnError(codes.Unknown, "cannot receive chunk data: %v", err)
		}

		chunk, size := request.GetChunk(), len(request.GetChunk())

		logger.Get().Info("received chunk", logger.Int("size", size))

		fileSize += size

		if fileSize > maxFileSize {
			return logAndReturnError(codes.InvalidArgument, "file is too large: %d > %d", fileSize, maxFileSize)
		}

		_, err = fileData.Write(chunk)
		if err != nil {
			return logAndReturnError(codes.Internal, "cannot write chunk data: %v", err)
		}
	}

	fileName, filePath, err := util.SaveStreamedDataToFile(s.cfg.UploadFolder, uploadInfo.GetFileType(), fileData.Bytes())

	if err != nil {
		return logAndReturnError(codes.Internal, "could not save file: %v", err)
	}

	upload := &repository.Upload{
		UserID:   uploadInfo.GetUserId(),
		Name:     uploadInfo.GetFileName(),
		FileName: fileName,
		FilePath: filePath,
	}

	id, err := s.store.UploadRepo().Create(upload)
	if err != nil {
		return logAndReturnError(codes.Internal, "could not save file: %v", err)
	}

	response := &pbV1.UploadImageResponse{
		Id:       id,
		Name:     upload.Name,
		FileName: upload.FileName,
	}

	err = stream.SendAndClose(response)
	if err != nil {
		return logAndReturnError(codes.Internal, "could not return response: %v", err)
	}

	return nil
}

package grpc

import (
	"context"

	pbV1 "gitlab.com/img_project/img_go_upload_service/protobuf/upload_service/v1"
	"google.golang.org/grpc/codes"
)

func (s *service) List(ctx context.Context, listRequest *pbV1.ListImageRequest) (*pbV1.ListImageResponse, error) {
	count, list, err := s.store.UploadRepo().GetAll(listRequest.GetUserId(), listRequest.GetLimit(), listRequest.GetOffset())

	if err != nil {
		return &pbV1.ListImageResponse{}, logAndReturnError(codes.Internal, "unexpected error: %v", err)
	}

	var converted []*pbV1.UploadImageResponse

	for _, item := range list {
		convertedItem := &pbV1.UploadImageResponse{
			Id:       item.ID,
			Name:     item.Name,
			FileName: item.FileName,
		}

		converted = append(converted, convertedItem)
	}

	return &pbV1.ListImageResponse{
		Count: count,
		List:  converted,
	}, nil
}
